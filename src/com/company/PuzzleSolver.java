package com.company;

import java.util.*;

/**
 * Created by christian on 14/12/15.
 */
public class PuzzleSolver {
    static class Solution implements Comparable<Solution> {
        private int weight;
        private Solution parent;
        private Board board;

        public Solution(Solution parent, Board board, String desired) {
            this.parent = parent;
            this.board = board;
            calculateWeight(desired);
        }

        private void calculateWeight(String desired) {
            StringTokenizer tokenizer = new StringTokenizer(desired);

            int currentRow = 0;
            int currentCol = 0;
            while ( tokenizer.hasMoreTokens() ) {
                String tok = tokenizer.nextToken();
                if ( ! tok.equals("*") ) {
                    int expected = Integer.valueOf(tok);
                    int found    = board.getCell(currentRow, currentCol);

                    if ( found != expected ) {
                        int foundAtRow = 0;
                        int foundAtCol = 0;

                        searchCell:
                        for (int i = 0; i < board.getWidth(); i++) {
                            for (int j = 0; j < board.getHeight(); j++) {
                                if ( board.getCell(i, j) == expected ) {
                                    foundAtRow = i;
                                    foundAtCol = j;
                                    break searchCell;
                                }
                            }
                        }

                        weight += Math.abs(currentRow - foundAtRow) + Math.abs(currentCol - foundAtCol);
                    }
                }

                currentCol++;
                if ( currentCol == board.getWidth() ) {
                    currentCol = 0;
                    currentRow++;
                }
            }
        }

        public Board getBoard() {
            return board;
        }

        public int getWeight() {
            return weight;
        }

        @Override
        public int compareTo(Solution o) {
            return weight - o.getWeight();
        }

        public Solution getParent() {
            return parent;
        }
    }

    public ArrayDeque<Solution> solve(Board startBoardState, String desiredPattern) {
        HashMap<String, Boolean> vis = new HashMap<>();
        PriorityQueue<Solution> q = new PriorityQueue<>();

        q.offer(new Solution(null, startBoardState, desiredPattern));
        vis.put(startBoardState.getAsPattern(), true);

        ArrayDeque<Solution> sol = new ArrayDeque<>();
        while ( ! q.isEmpty() ) {
            Solution currentSolution = q.poll();

            Board board = currentSolution.getBoard();

            if ( board.getAsPattern().trim().equals(desiredPattern) ) {
                sol.push( currentSolution );

                while ( currentSolution.getParent() != null ) {
                    currentSolution = currentSolution.getParent();
                    sol.push(currentSolution);
                }

                return sol;
            }

            if ( board.canShiftHorizontalRight() ) {
                processNode(vis, q, currentSolution, board, desiredPattern);
                board.canShiftHorizontalLeft();
            }

            if ( board.canShiftHorizontalLeft() ) {
                processNode(vis, q, currentSolution, board, desiredPattern);
                board.canShiftHorizontalRight();
            }

            if ( board.canShiftVerticalUp() ) {
                processNode(vis, q, currentSolution, board, desiredPattern);
                board.shiftVerticalDown();
            }

            if ( board.shiftVerticalDown() ) {
                processNode(vis, q, currentSolution, board, desiredPattern);
                board.canShiftVerticalUp();
            }
        }

        return null;
    }


    private void processNode(HashMap<String, Boolean> vis, PriorityQueue<Solution> q, Solution parent, Board board, String desired) {
        String pattern = board.getAsPattern().trim();
        if ( ! vis.containsKey(pattern) ) {
            Board newBoard = (Board)board.clone();
            Solution solution = new Solution(parent, newBoard, desired);
            q.add(solution);
            vis.put(pattern, true);
        }
    }
}
