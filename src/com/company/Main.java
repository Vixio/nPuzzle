package com.company;

import java.util.ArrayDeque;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n;
        do {
            System.out.println("Longitud del tablero: ");
            n = scanner.nextInt();
        } while( n < 0 );
        scanner.nextLine();

        String sourcePattern;
        StringTokenizer tok;
        do {
            System.out.println("Patron de partida: ");
            sourcePattern = scanner.nextLine();
            tok = new StringTokenizer(sourcePattern);
        } while( tok.countTokens() != n * n );

        Board board = new Board(n, sourcePattern);

        String dstPattern;
        do {
            System.out.println("Patron de objetivo: ");
            dstPattern = scanner.nextLine();
            tok = new StringTokenizer(dstPattern);
        } while( tok.countTokens() != n * n );

        PuzzleSolver solver = new PuzzleSolver();

        long startTime = System.currentTimeMillis();

        System.out.println("Resolviendo..");
        ArrayDeque<PuzzleSolver.Solution> solution = solver.solve(board, dstPattern);
        int movs = 0;
        if ( solution != null ) {
            movs = solution.size();

            while (!solution.isEmpty()) {
                PuzzleSolver.Solution move = solution.pop();
                System.out.println(move.getBoard());
                System.out.println("----------------------");
            }
        }

        // 1 2 3
        // 4 * 6
        // 7 8 5

        // 1 2 3
        // 4 8 6
        // 7 * 5
        long elapsedTime = System.currentTimeMillis()  - startTime;

        System.out.println("Número de movimientos necesarios: " + movs);
        System.out.println("Elapsed time: " + (elapsedTime));
    }

}
