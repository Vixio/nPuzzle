package com.company;

import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Created by christian on 26/11/15.
 */
public class Board implements Cloneable {
    private static final int EMPTY_CELL = 0;

    private int emptyRow;
    private int emptyCol;
    private int data[][];

    public Board(int n, String pattern) {
        initializeBoard(n);
        populate(n, pattern);
    }

    private void initializeBoard(int n) {
        if ( data == null || n != data.length ) {
            data = new int[n][n];
        }

        for ( int i = 0; i < data.length; i++ ) {
            for ( int j = 0; j < data[i].length; j++ ) {
                data[i][j] = EMPTY_CELL;
            }
        }
    }

    public void populate(int n, String pattern) {
        if ( data.length != n ) {
            initializeBoard(n);
        }

        StringTokenizer tokenizer = new StringTokenizer(pattern);
        for (int i = 0; i < data.length; i++) {
            for ( int j = 0; j < data[i].length; j++ ) {
                if ( tokenizer.hasMoreTokens() ) {
                    String tok = tokenizer.nextToken();
                    if ( tok.equals("*") ) {
                        emptyRow = i;
                        emptyCol = j;
                        data[i][j] = EMPTY_CELL;
                    } else {
                        data[i][j] = Integer.valueOf(tok);
                    }
                }
            }
        }
    }

    public boolean canShiftHorizontalLeft() {
        return switchCells(emptyRow, emptyCol, emptyRow, emptyCol - 1);
    }

    public boolean canShiftHorizontalRight() {
        return switchCells(emptyRow, emptyCol, emptyRow, emptyCol + 1);
    }

    public boolean canShiftVerticalUp() {
        return switchCells(emptyRow, emptyCol, emptyRow - 1, emptyCol);
    }

    public boolean shiftVerticalDown() {
        return switchCells(emptyRow, emptyCol, emptyRow + 1, emptyCol);
    }

    private boolean switchCells(int row1, int col1, int row2, int col2) {
        try {
            int cell1 = data[row1][col1];
            data[row1][col1] = data[row2][col2];
            data[row2][col2] = cell1;

            emptyRow = row2;
            emptyCol = col2;
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public int getHeight() {
        return data.length;
    }

    public int getWidth() {
        return data[0].length;
    }

    public int getCell(int row, int col) {
        return data[row][col];
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for ( int i = 0; i < data.length; i++) {
            for (int j=0; j <  data[i].length; j++) {
                builder.append(data[i][j]);
                builder.append('\t');
            }
            //builder.append(Arrays.toString(data[i]));
            builder.append('\n');
        }

        return builder.toString();
    }

    public String getAsPattern() {
        StringBuilder builder = new StringBuilder();

        for ( int i = 0; i < data.length; i++) {
            for ( int j = 0; j < data[i].length; j++) {

                if ( data[i][j] == 0 ) {
                    builder.append("*");
                } else {
                    builder.append(data[i][j]);
                }

                if ( i == data.length ) {
                    return builder.toString();
                }

                builder.append(" ");
            }
        }

        return builder.toString();
    }

    @Override
    protected Object clone() {
        return new Board(data.length, getAsPattern());
    }
}
